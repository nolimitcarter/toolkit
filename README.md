# My toolkit 

### A repo comprised of useful tools, wordlists, and scripts to use when pen-testing

### Contents: 

* numerous recon scripts/methods
* port-scanning scripts, udp and tcp
* recon scripts, hostname, web, whois 
* ssh tunnels/ reverse shells
* dos/ddos attacks/tools
* website ip trackers
* wifi, wpa2
* windows admin management 
* wordlists 
* python scripts
* general notes

