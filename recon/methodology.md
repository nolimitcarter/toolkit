# The Bug Hunter's Methodology v4.0 - Recon Edition by @jhaddix

### Wide recon is the art of discovering as many assets related to a target as possible. Only is scope permits. 

## One of the main goals is to be able to automate this process as much as possible.

* Scope Domains
	* targets
* Acquisitions
	* crunchbase.com
	* other business info sites
	* again check the scope to see if other domains are in scope
* ASN(Autonomous System Numbers) Enumeration
	* track down related sites
	* bgp.he.net (Hurricane Electric's free-form search)
	* Amass - for discovering more seed domains we want to scan the whole ASN with a port scanner and return any root domains 	    we see in SSL certificates, etc. We can do this with amass. 
* Reverse WHOIS
	* whoxy.com - lets you search a domain (etc: twitch.tv) and pulls up who has owned it in the past.
	* DOMLink - will recursively query the WHOXY WHOIS API.
	* builtwith.com - tech profiling. The relationship profile tab in this shows a listing of all the analytics tracker codes 	that the target uses. It could show other sites that could be within our scope. 
  * You can also use Google-Fu to google the copyright text, terms of service text, and privacy policy from a main target to glean related hosts on google.
  * Shodan - tool that continuously spiders infrastructure on the internet. It is much more verbose than regular spiders. It captures response data, cert data, stack profiling data, and more. 
	* maybe other tools?
* Subdomain Enumeration
  * Linked and JS Discovery
    * Burp Suite Pro
    * GoSpider
    * hakrawler
    * Subdomainizer 
  * Subdomain Scraping
    * Google example: 
      1. site:twitch.tv -www.twitch.tv 
      2. site:twitch.tv -www.twitch.tv -watch.twitch.tv
      3. site:twitch.tv -www.twitch.tv -watch.twitch.tv -dev.twitch.tv
      4. ...etc
    * Amass 
    * Subfinder 
    * github-subdomains.py (found in github-search)
    * showsubgo - parser for Shodan
    * Cloud Ranges - masscan
  * Subdomain Bruteforce (make sure to use a good wordlist ex: bruteforce/subdomains/all.txt)
    * massdns
    * amass `-rf` flag
      * amass offers bruteforcing via the "enum" tool using the "brute" switch. Example: `amass enum -brute -d twitch.tv -src` You can also specify any number of resolvers: `amass enum -brute -d twitch.tv -rf resolvers.txt -w bruteforce.list`
    * shuffleDNS
    * altdns
* Port Analysis
  * nmap 
  * masscan - only scans IPs (apparently much faster than nmap) 
    * sample input syntax for scanning a list of IPs:
      `masscan -p1-65535 -iL $ipFile --max-rate 1800 -oG $outPutFile.log`
  * dnmasscan - adds ability to do a resolve on a domain, get the IP address and pass it to masscan
    * sample input syntax:
      `dnmasscan example.txt dns.log -p80,443 -oG masscan.log`
  * brutespray - (service scanning) Scans the remote protocols for default passwords which can take in some of out earlier output files.
  * GitHub Dorking
    * https://gist.github.com/jhaddix/1fb7ab2409ab579178d2a79959909b33
* Extras: 
  * subdomain takeovers - a vulnerability that occurs when a subdomain (subdomain.example.com) is pointing to a service (GitHub pages, Heroku, etc.) that has been removed or deleted. This allows an attacker to set up a page on the service that was being used and point their page to that subdomain. For example, if subdomain.example.com was pointing to a GitHub page and the user decided to delete their GitHub page, an attacker can now create a GitHub page, add a CNAME file containing subdomain.example.com, and claim it. Good resource: GitHub repo can-i-take-over-xyz. 
    * Nuclei subdomain-takeover 
  * interlace by Micheal Skelton aka Codingo
  * tomnomnom tools


# Definitions/ tool exmplanations:

* ASN - numbers given to large enough networks. These ASN's will help us track down some semblance of an entity's IT infrastructure. They basically allow us to view IPs related to the target we might not have seen before. 
* Amass - example usage: amass intel -asn 46489 
* Google-Fu - example usage: "&c 2019 Twitch Interactive, Inc." inurl:twitch 
* Linked and JS Discovery Subdomain Enumeration - Allows us to visit a seed/root and recursively spider all the links for a term with regex, examining those links and allows us to add things to scope. ( 1. Turn off passive scanning 2. Set forms auto to submit 3. Set scope to advanced control and use "keyword" of target name 4. Walk and browse main site, then spider all hosts recursively until we have a big list.) 
* GoSpider - spider that can be used for many things and supports parsing JS very well. 
* Hakrawler - spider is very similar to GoSpider but has a lot more interesting parsing.
* Subdomainizer - visits the JS referenced on the file/link and starts parsing out subdomains out of the js files. Is able to find subdomains referenced in js files, cloud services referenced in js files, and uses the Shannon Entropy formula to find potentially sensitive items in the js files. 
* github-subdomains.py 
  * example usage: 
  `python3 github-subdomains.py -t "` 
  `cat twitch.tv |wc -l`
  `cat twitch.tv | grep -v ".tmi"` *shows subdomains*
* GitHub Dorking - Sooner or later a new dev, intern, contractor, or other staff will leak source code online, sometimes through a public github.com repo that they mistakenly thought they had set private. The script listed above and scripts in github-search can help with this.
