#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Please run this script as root." 1>&2
  exit 1
fi

if [ $# -eq 1 ]; then
    nmap -sU -vv --top-ports 1000 --reason --open -oA udp_ports_full_$1 $1
else
    echo "Please provide the target IP address or an IP range."
fi
