#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Please run this script as root." 1>&2
  exit 1
fi

if [ $# -eq 1 ]; then
	nmap -sS -vv --top-ports 1000 --open -oA tcp_ports_1000_$1 $1
else
    echo "Please provide the target IP address or an IP range."
fi
