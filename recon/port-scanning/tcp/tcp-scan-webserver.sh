#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Please run this script as root." 1>&2
  exit 1
fi

if [ $# -eq 1 ]; then
	nmap -sS -vv -p80,443 -O -T4 -Pn --reason -oA tcp_web_server_$1 $1
else
    echo "Please provide the web server."
fi
