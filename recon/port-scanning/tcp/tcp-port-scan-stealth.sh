#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "Please run this script as root." 1>&2
  exit 1
fi

if [ $# -eq 1 ]; then
	nmap -sS -n -T1 -Pn -vv -p21,22,25,53,80,110,135,137,139,143,443,445,465,587,981,993,995,1194,1433,2525,3306,3389,5060,5061,7777,8006,8008,8080,8090,8333,8880,8888,8443,9000,9001 --reason --open --max-rate 1 --host-timeout 30m --scan-delay 1s --max-retries 1 -oA tcp_top_ports_$1 $1
else
    echo "Please provide the target IP address or an IP range."
fi
